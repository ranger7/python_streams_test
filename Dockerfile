FROM ubuntu:latest
RUN apt-get update
RUN apt-get install python3 python3-pip -y
WORKDIR /app
COPY test.py .
ADD keysight keysight/
COPY ./requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt
CMD ["sh", "-c", "tail -f /dev/null"]
# CMD ["python3", "test.py"]