#!/bin/sh

set -ex

cd "$(dirname $0)"
DOCKER_IMAGE="python-test:2.0"

docker build -t "${DOCKER_IMAGE}" . 
docker run --rm -it \
	"${DOCKER_IMAGE}" \
	"$@"
