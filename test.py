#! /usr/bin/python

import unittest

# import ranger_api_schema
import os.path

import grpc

import keysight.dplane.ranger.l23access.stream.v1.stream_pb2 as stream_pb_2
import keysight.dplane.ranger.l23access.mac_sec.v1.mac_sec_pb2 as mac_sec_pb2
import keysight.dplane.ranger.l23access.stream.v1.stream_pb2_grpc as stream_pb2_grpc
import keysight.dplane.ranger.l23access.table_udf.v1.table_udf_pb2 as table_udf_pb2
import keysight.dplane.ranger.l23access.table_udf.v1.table_udf_pb2_grpc
import keysight.dplane.ranger.l23access.udf.v1.udf_pb2 as udf_pb2
import keysight.dplane.ranger.l23access.pf_type.v1.pf_type_pb2 as pf_type_pb2
import keysight.dplane.ranger.l23access.id_type.v1.id_type_pb2 as id_type_pb2
# now import modules as required
# import ranger_api_schema.keysight.ranger.fpga_controller.v1.fpga_controller_pb2 as fpga_controller_pb2
# import ranger_api_schema.keysight.ranger.fpga_controller.v1.fpga_controller_pb2_grpc as fpga_controller_pb2_grpc

# ! /usr/bin/python
# import capture.v1.capture_pb2
# import capture.v1.capture_pb2_grpc
# import data_integrity_rx.v1.data_integrity_rx_pb2
# import data_integrity_rx.v1.data_integrity_rx_pb2_grpc
# import debug_topology.v1.debug_topology_pb2
# import debug_topology.v1.debug_topology_pb2_grpc
# import feature.v1.feature_pb2
# import feature.v1.feature_pb2_grpc
# import id_type.v1.id_type_pb2_grpc
# import mac_sec.v1.mac_sec_pb2_grpc
# import packet_group.v1.packet_group_pb2
# import packet_group.v1.packet_group_pb2_grpc

# import pf_type.v1.pf_type_pb2_grpc
# import port.v1.port_pb2
# import port.v1.port_pb2_grpc
# import rate_monitoring.v1.rate_monitoring_pb2
# import rate_monitoring.v1.rate_monitoring_pb2_grpc
# import rx_auto_instrumentation.v1.rx_auto_instrumentation_pb2
# import rx_auto_instrumentation.v1.rx_auto_instrumentation_pb2_grpc
import keysight.dplane.ranger.l23access.server.v1.server_pb2 as server_pb2

import keysight.dplane.ranger.l23access.server.v1.server_pb2_grpc as server_pb2_grpc
# import split_packet_group.v1.split_packet_group_pb2
# import split_packet_group.v1.split_packet_group_pb2_grpc
# import stat_catalog.v1.stat_catalog_pb2
# import stat_catalog.v1.stat_catalog_pb2_grpc
# import udf.v1.udf_pb2_grpc


class TestEthernet(unittest.TestCase):
    def __init__(self):
        super().__init__()
        self.NUM_TEST_STREAM = 100
        self.NUM_FP_PORTS = 1
        self.stream_port = str(7555)
        self.address = 'nrs-v1-nanite-rpc-server.default.svc.cluster.local'

        self.fillProtocolHeaderInfoList()
        self.fillSpecialProtocolInfoType()
        self.fillProtocolHeaderInfoListType()
        self.fillBackgroundOverlayList()
        
        self.fillUdfTable()
        self.fillSequenceUdfConfiguration()
        self.fillUdfConfiguration()
        self.fillMacSecStreamControlType()
        self.fillAutodetectSignatureTxType()
        self.fillDataIntegrityTxType()
        self.fillFrameControlType()
        self.fillSrcAddrControlType()
        self.fillDstAddrControlType()
        self.fillRateControlType()
        self.fillRateControlType()
        self.fillStreamControlType()
        self.fillInterStreamGapControlType()
        self.fillInterFrameGapControlType()
        self.fillInterBurstGapControlType()
        self.fillConfigurationDataType()

    def fillStreamControlType(self):
        self.stream_control_type = stream_pb_2.StreamControlType()
        self.stream_control_type.dma = stream_pb_2.EDma.EDMA_STOP_STREAM
        stream_id = id_type_pb2.StreamId()
        stream_id.id = 1
        self.stream_control_type.return_to_id.CopyFrom(stream_id)
        self.stream_control_type.loop_count = 1
        self.stream_control_type.num_bursts = 1
        self.stream_control_type.num_frames = 100
        self.stream_control_type.priority_group = stream_pb_2.EPriorityGroup.EPRIORITY_GROUP_PRIORITY_GROUP0
        # If feature is active, set True
        self.stream_control_type.tsn_qbv_gate_config.enabled = False
        self.stream_control_type.async_int_enable = True
        # self.stream_control_type.adjust_mask =
        # stream_control_type.stream_control_type =
        self.stream_control_type.enable_incr_frame_burst_override = False
        self.stream_control_type.enable_source_interface = False
        self.stream_control_type.enable_statistic = True
        self.stream_control_type.enable_suspend = False
        self.stream_control_type.rx_trigger_enable = False
        # stream_control_type.start_tx_delay

    def fillRateControlType(self):
        self.rate_control_type = stream_pb_2.RateControlType()
        self.rate_control_type.rate_mode = stream_pb_2.ERateMode.ERATE_MODE_STREAM_RATE_MODE_PERCENT_RATE
        percent_type = pf_type_pb2.PercentType()
        # ??? percent_type.percent = 0.9893041849136355
        self.rate_control_type.percent_packet_rate.CopyFrom(percent_type)
        self.rate_control_type.fps_rate = 80405.08655019794
        self.rate_control_type.bps_rate = 976439371.0656039

    def fillInterFrameGapControlType(self):
        self.inter_frame_gap_control_type = stream_pb_2.InterFrameGapControlType()
        self.inter_frame_gap_control_type.ifg_type = stream_pb_2.EIfgType.EIFG_TYPE_GAP_FIXED
        gap_time = pf_type_pb2.GapTimeType()
        gap_time.duration = 12315
        gap_time.units = pf_type_pb2.EGapUnit.EGAP_UNIT_GAP_NANO_SECONDS
        self.inter_frame_gap_control_type.ifg.CopyFrom(gap_time)
        gap_time.duration = 24
        self.inter_frame_gap_control_type.ifg_min.CopyFrom(gap_time)
        gap_time.duration = 32
        self.inter_frame_gap_control_type.ifg_max.CopyFrom(gap_time)
        # ??? self.inter_frame_gap_control_type.ifg_precise =

    def fillInterBurstGapControlType(self):
        self.inter_burst_gap_control_type = stream_pb_2.InterBurstGapControlType()
        # If feature is active, set True
        self.inter_burst_gap_control_type.enable_ibg = False

    def fillInterStreamGapControlType(self):
        self.inter_stream_gap_control_type = stream_pb_2.InterStreamGapControlType()
        # If feature is active, set True
        self.inter_stream_gap_control_type.enable_isg = False

    def fillDstAddrControlType(self):
        type = pf_type_pb2.MacAddressType()
        type.address = 256
        self.dst_addr_control_type = stream_pb_2.DstAddrControlType()
        self.dst_addr_control_type.dst_addr.CopyFrom(type)
        type.address = 0
        self.dst_addr_control_type.dst_addr_mask_select.CopyFrom(type)
        self.dst_addr_control_type.dst_addr_mask_value.CopyFrom(type)
        self.dst_addr_control_type.dst_addr_repeat_counter = \
            stream_pb_2.EAddressRepeatCounter.EADDRESS_REPEAT_COUNTER_IDLE
        self.dst_addr_control_type.dst_addr_step = 1
        self.dst_addr_control_type.num_dst_addr = 1
        self.dst_addr_control_type.enable_dst_addr_continue_from_last_value = False

    def fillSrcAddrControlType(self):
        self.src_addr_control_type = stream_pb_2.SrcAddrControlType()
        type = pf_type_pb2.MacAddressType()
        type.address = 90520732302350
        self.src_addr_control_type.src_addr.CopyFrom(type)
        type.address = 0
        self.src_addr_control_type.src_addr_mask_select.CopyFrom(type)
        self.src_addr_control_type.src_addr_mask_value.CopyFrom(type)
        self.src_addr_control_type.src_addr_repeat_counter = \
            stream_pb_2.EAddressRepeatCounter.EADDRESS_REPEAT_COUNTER_IDLE
        self.src_addr_control_type.src_addr_step = 1
        self.src_addr_control_type.num_src_addr = 1
        self.src_addr_control_type.enable_src_continue_from_last_value = False

    # def fillFramePreemptionType(self):
    #     self.frame_preemption_type = stream_pb_2.FramePreemptionType()
    #     self.frame_preemption_type.smd_type =
    #     self.frame_preemption_type.frag_count
    #     self.frame_preemption_type.crc_type

    def fillFrameControlType(self):
        self.frame_control_type = stream_pb_2.FrameControlType()
        # enableFramePreemption = False ??
        # self.frame_control_type.frame_preemption_type =
        self.frame_control_type.frame_size_type = stream_pb_2.EFrameSizeType.EFRAME_SIZE_TYPE_SIZE_FIXED

        # ?? is of type int32
        # self.frame_control_type.frame_type = "FF FF"
        self.frame_control_type.frame_size = 1518
        fixed_point = pf_type_pb2.FixedPoint16()
        fixed_point.value = 1518
        self.frame_control_type.average_frame_size.CopyFrom(fixed_point)
        self.frame_control_type.frame_size_step = 1
        # ?? self.frame_control_type.frame_count =
        self.frame_control_type.frame_size_max = 1518
        self.frame_control_type.frame_size_min = 1518
        self.frame_control_type.enforce_min_gap = 12
        self.frame_control_type.preamble_data = b'8'
        self.frame_control_type.pattern_type = stream_pb_2.EPatternType.EPATTERN_TYPE_INCR_BYTE
        self.frame_control_type.fcs_error = stream_pb_2.EFcsError.EFCS_ERROR_STREAM_ERROR_GOOD
        self.frame_control_type.enable_timestamp = False
        # ??? self.frame_control_type.random_size_control_type =

   

    def fillSpecialProtocolInfoType(self):
        self.special_protocol_info_type = stream_pb_2.SpecialProtocolInfoType()
        self.special_protocol_info_type.enable_isl = False
        self.special_protocol_info_type.enable_data_mangling = False
        self.special_protocol_info_type.enable_fcoe = False
        # self.special_protocol_info_type.force_valid_f_co_e_size =
        self.special_protocol_info_type.enable_vntag = False
        # self.special_protocol_info_type.fcoe_eof
        # self.special_protocol_info_type.fcoe_reserved1
        # self.special_protocol_info_type.fcoe_reserved2
        # self.special_protocol_info_type.fcoe_reserved3

    def fillProtocolHeaderInfoList(self):
        self.protocol_header_info_list = stream_pb_2.ProtocolHeaderInfoList()
        # ??? items is a list of ProtocolHeaderInfoType
        # self.protocol_header_info_list.items

    def fillBackgroundOverlayList(self):
        self.background_overlay_list = stream_pb_2.BackgroundOverlayList()

    def fillProtocolHeaderInfoListType(self):
        self.protocol_header_info_list_type = stream_pb_2.ProtocolHeaderInfoListType()
        self.protocol_header_info_list_type.protocol_header.CopyFrom(self.protocol_header_info_list)
        # self.protocol_header_info_list_type.protocol_header = self.protocol_header_info_list
        self.protocol_header_info_list_type.special_protocol_info.CopyFrom(self.special_protocol_info_type)
        

    def fillUdfTable(self):
        self.table_configuration_type = table_udf_pb2.TableUdfConfigurationType()
        # ??? self.table_configuration_type.rows =
        self.table_configuration_type.columns.CopyFrom(table_udf_pb2.ColumnSettingList())

    def fillUdfConfiguration(self):
        self.udf_configuration_type = udf_pb2.UdfConfigurationType()
        self.udf_configuration_type.udf_list.CopyFrom(udf_pb2.UdfList())

    def fillSequenceUdfConfiguration(self):
        self.sequence_udf_configuration_type = udf_pb2.SequenceUdfConfigurationType()
        self.sequence_udf_configuration_type.enable = False

    def fillMacSecStreamControlType(self):
        self.mac_sec_stream_control_type = mac_sec_pb2.MacSecStreamControlType()
        self.mac_sec_stream_control_type.enable_mac_sec = False

    def fillAutodetectSignatureTxType(self):
        self.autodetect_signature_type = stream_pb_2.AutodetectSignatureTxType()
        self.autodetect_signature_type.insert_signature = False
        self.autodetect_signature_type.signature = str.encode("08 71 18 05")
        self.autodetect_signature_type.signature_offset = 48

    def fillDataIntegrityTxType(self):
        self.data_integrity_type = stream_pb_2.DataIntegrityTxType()
        self.data_integrity_type.enable = False

    def fillConfigurationDataType(self):
        self.configuration_data_type = stream_pb_2.ConfigurationDataType()
        self.configuration_data_type.stream_control.CopyFrom(self.stream_control_type)
        self.configuration_data_type.inter_frame_gap_control.CopyFrom(self.inter_frame_gap_control_type)
        self.configuration_data_type.rate_control.CopyFrom(self.rate_control_type)
        self.configuration_data_type.inter_burst_gap_control.CopyFrom(self.inter_burst_gap_control_type)
        self.configuration_data_type.inter_stream_gap_control.CopyFrom(self.inter_stream_gap_control_type)
        # Unsure if the address is correct
        self.configuration_data_type.dst_addr_control.CopyFrom(self.dst_addr_control_type)
        self.configuration_data_type.src_addr_control.CopyFrom(self.src_addr_control_type)
        self.configuration_data_type.frame_control.CopyFrom(self.frame_control_type)
        #
        # self.configuration_data_type.bgnd_data_control = self.background_data_control_type
        self.configuration_data_type.protocol_headers.CopyFrom(self.protocol_header_info_list_type)
        self.configuration_data_type.autodetect_signature_tx.CopyFrom(self.autodetect_signature_type)
        self.configuration_data_type.mac_sec.CopyFrom(self.mac_sec_stream_control_type)
        self.configuration_data_type.data_integrity_tx.CopyFrom(self.data_integrity_type)
        self.configuration_data_type.udf_configuration.CopyFrom(self.udf_configuration_type)
        self.configuration_data_type.sequence_udf_configuration.CopyFrom(self.sequence_udf_configuration_type)
        self.configuration_data_type.table_udf_configuration.CopyFrom(self.table_configuration_type)

    def create_stream_write_request(self):
        write_request = server_pb2.ConfigureStreamsRequest()
        port_id = id_type_pb2.PortId()
        port_id.id = 1
        write_request.port_id.CopyFrom(port_id)
        stream_config = stream_pb_2.TrafficConfigurationType()
        stream_config.gap_control_mode = stream_pb_2.EGapControlMode.EGAP_CONTROL_MODE_STREAM_GAP_CONTROL_FIXED
    #    stream_config.streams = A list of configurations for each of the streams

        items = []
        print("No of Streams ----- ", self.NUM_TEST_STREAM)
        for port in range(self.NUM_FP_PORTS):
            for no_stream in range(self.NUM_TEST_STREAM):
                # stream_config.streams contains elements of type StreamConfigurationType
                item = stream_pb_2.StreamConfigurationType()
                items.append(item)
                items[no_stream] = stream_config.streams.items.add()
                stream_id = id_type_pb2.StreamId()
                stream_id.id = no_stream
                items[no_stream].stream_id.CopyFrom(stream_id)
                items[no_stream].name = f"steam no {no_stream}"
                items[no_stream].state = stream_pb_2.EState.ESTATE_ACTIVE
                items[no_stream].configuration_data.CopyFrom(self.configuration_data_type) 
        print(f"length = {len(stream_config.streams.items)}")
        write_request.stream_configuration.CopyFrom(stream_config)
        return write_request
    
    def create_streams_data_begin_request(self):
        streams_data_begin_request = server_pb2.ConfigureStreamsDataBeginRequest()
        port_id = id_type_pb2.PortId()
        port_id.id = 1
        streams_data_begin_request.port_id.CopyFrom(port_id)

        background_data = stream_pb_2.BackgroundDataConfigurationType()

        items = []
        for port in range(self.NUM_FP_PORTS):
            for no_stream in range(self.NUM_TEST_STREAM):
                 #background_data_configuration_type.setting.items is a list of BackgroundDataSettingType
                bgnd_setting = stream_pb_2.BackgroundDataSettingType()
                items.append(bgnd_setting)
                stream_id = id_type_pb2.StreamId()
                stream_id.id = no_stream
                items[no_stream].stream_id.CopyFrom(stream_id)
                bgnd_data = stream_pb_2.BackgroundDataType()
                # ???
                # bgnd_data.protocol = 
                bgnd_data.pattern = str.encode("00 01 02 03")
                items[no_stream].background_data.CopyFrom(bgnd_data)
                items[no_stream] = self.background_data_configuration_type.setting.items.add()
        
        streams_data_begin_request.background_data.CopyFrom(background_data)
        return streams_data_begin_request

    def runTest(self):
        with grpc.insecure_channel(self.address + ':' + self.stream_port) as stream_channel:
            streamStub = server_pb2_grpc.ServerServiceStub(stream_channel)
            # init_request = server_pb2.InitRequestRequest()
            
            
            print("------------Creating Ranger Stream------------")
            write_request = self.create_stream_write_request()
            response = streamStub.InitRequest(write_request)
            streamStub.ConfigureStreams(write_request)

            print("------------Creating Data Begin Stream------------")
            streams_data_begin_request = self.create_streams_data_begin_request()
            response = streamStub.InitRequest(streams_data_begin_request)
            streamStub.ConfigureStreams(streams_data_begin_request)

if __name__ == '__main__':
    test = TestEthernet()
    test.runTest()
    # unittest.main()

