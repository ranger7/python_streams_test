# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: keysight/dplane/ranger/l23access/stat_catalog/v1/stat_catalog.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from keysight.dplane.ranger.l23access.id_type.v1 import id_type_pb2 as keysight_dot_dplane_dot_ranger_dot_l23access_dot_id__type_dot_v1_dot_id__type__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\nCkeysight/dplane/ranger/l23access/stat_catalog/v1/stat_catalog.proto\x12\x30keysight.dplane.ranger.l23access.stat_catalog.v1\x1a\x39keysight/dplane/ranger/l23access/id_type/v1/id_type.proto\"\xc0\x02\n\x10StHtgCatalogStat\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x02 \x01(\t\x12\x0f\n\x07stat_id\x18\x03 \x01(\x05\x12\x18\n\x10stat_engine_path\x18\x04 \x01(\t\x12U\n\x0fstat_value_type\x18\x05 \x01(\x0e\x32<.keysight.dplane.ranger.l23access.stat_catalog.v1.EValueType\x12N\n\tstat_type\x18\x06 \x01(\x0e\x32;.keysight.dplane.ranger.l23access.stat_catalog.v1.EStatType\x12\x11\n\tindex_min\x18\x07 \x01(\x05\x12\x11\n\tindex_max\x18\x08 \x01(\x05\x12\x11\n\tmode_list\x18\t \x01(\t\"\xee\x01\n\x10StHtgCatalogItem\x12\x44\n\x07port_id\x18\x01 \x01(\x0b\x32\x33.keysight.dplane.ranger.l23access.id_type.v1.PortId\x12\x13\n\x0bsource_type\x18\x02 \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x03 \x01(\t\x12\x0f\n\x07item_id\x18\x04 \x01(\x05\x12Y\n\tstat_list\x18\x05 \x01(\x0b\x32\x46.keysight.dplane.ranger.l23access.stat_catalog.v1.StHtgCatalogStatList\"i\n\x14StHtgCatalogStatList\x12Q\n\x05items\x18\x01 \x03(\x0b\x32\x42.keysight.dplane.ranger.l23access.stat_catalog.v1.StHtgCatalogStat*S\n\nEValueType\x12\x17\n\x13\x45VALUE_TYPE_COUNTER\x10\x00\x12\x16\n\x12\x45VALUE_TYPE_STRING\x10\x01\x12\x14\n\x10\x45VALUE_TYPE_PAIR\x10\x02*9\n\tEStatType\x12\x16\n\x12\x45STAT_TYPE_DEFAULT\x10\x00\x12\x14\n\x10\x45STAT_TYPE_ARRAY\x10\x01\x32\x14\n\x12StatCatalogServiceBzZugitlab.it.keysight.com/ranger/ranger-api-schema/pkg/api/keysight/dplane/ranger/l23access/stat_catalog/v1;stat_catalog\xf8\x01\x01\x62\x06proto3')

_EVALUETYPE = DESCRIPTOR.enum_types_by_name['EValueType']
EValueType = enum_type_wrapper.EnumTypeWrapper(_EVALUETYPE)
_ESTATTYPE = DESCRIPTOR.enum_types_by_name['EStatType']
EStatType = enum_type_wrapper.EnumTypeWrapper(_ESTATTYPE)
EVALUE_TYPE_COUNTER = 0
EVALUE_TYPE_STRING = 1
EVALUE_TYPE_PAIR = 2
ESTAT_TYPE_DEFAULT = 0
ESTAT_TYPE_ARRAY = 1


_STHTGCATALOGSTAT = DESCRIPTOR.message_types_by_name['StHtgCatalogStat']
_STHTGCATALOGITEM = DESCRIPTOR.message_types_by_name['StHtgCatalogItem']
_STHTGCATALOGSTATLIST = DESCRIPTOR.message_types_by_name['StHtgCatalogStatList']
StHtgCatalogStat = _reflection.GeneratedProtocolMessageType('StHtgCatalogStat', (_message.Message,), {
  'DESCRIPTOR' : _STHTGCATALOGSTAT,
  '__module__' : 'keysight.dplane.ranger.l23access.stat_catalog.v1.stat_catalog_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.stat_catalog.v1.StHtgCatalogStat)
  })
_sym_db.RegisterMessage(StHtgCatalogStat)

StHtgCatalogItem = _reflection.GeneratedProtocolMessageType('StHtgCatalogItem', (_message.Message,), {
  'DESCRIPTOR' : _STHTGCATALOGITEM,
  '__module__' : 'keysight.dplane.ranger.l23access.stat_catalog.v1.stat_catalog_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.stat_catalog.v1.StHtgCatalogItem)
  })
_sym_db.RegisterMessage(StHtgCatalogItem)

StHtgCatalogStatList = _reflection.GeneratedProtocolMessageType('StHtgCatalogStatList', (_message.Message,), {
  'DESCRIPTOR' : _STHTGCATALOGSTATLIST,
  '__module__' : 'keysight.dplane.ranger.l23access.stat_catalog.v1.stat_catalog_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.stat_catalog.v1.StHtgCatalogStatList)
  })
_sym_db.RegisterMessage(StHtgCatalogStatList)

_STATCATALOGSERVICE = DESCRIPTOR.services_by_name['StatCatalogService']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'Zugitlab.it.keysight.com/ranger/ranger-api-schema/pkg/api/keysight/dplane/ranger/l23access/stat_catalog/v1;stat_catalog\370\001\001'
  _EVALUETYPE._serialized_start=851
  _EVALUETYPE._serialized_end=934
  _ESTATTYPE._serialized_start=936
  _ESTATTYPE._serialized_end=993
  _STHTGCATALOGSTAT._serialized_start=181
  _STHTGCATALOGSTAT._serialized_end=501
  _STHTGCATALOGITEM._serialized_start=504
  _STHTGCATALOGITEM._serialized_end=742
  _STHTGCATALOGSTATLIST._serialized_start=744
  _STHTGCATALOGSTATLIST._serialized_end=849
  _STATCATALOGSERVICE._serialized_start=995
  _STATCATALOGSERVICE._serialized_end=1015
# @@protoc_insertion_point(module_scope)
