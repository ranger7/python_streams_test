# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: keysight/dplane/ranger/l23access/packet_group/v1/packet_group.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from keysight.dplane.ranger.l23access.split_packet_group.v1 import split_packet_group_pb2 as keysight_dot_dplane_dot_ranger_dot_l23access_dot_split__packet__group_dot_v1_dot_split__packet__group__pb2
from keysight.dplane.ranger.l23access.id_type.v1 import id_type_pb2 as keysight_dot_dplane_dot_ranger_dot_l23access_dot_id__type_dot_v1_dot_id__type__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\nCkeysight/dplane/ranger/l23access/packet_group/v1/packet_group.proto\x12\x30keysight.dplane.ranger.l23access.packet_group.v1\x1aOkeysight/dplane/ranger/l23access/split_packet_group/v1/split_packet_group.proto\x1a\x39keysight/dplane/ranger/l23access/id_type/v1/id_type.proto\"\x9f\x01\n\tPGIDRange\x12I\n\x05start\x18\x01 \x01(\x0b\x32:.keysight.dplane.ranger.l23access.id_type.v1.PacketGroupId\x12G\n\x03\x65nd\x18\x02 \x01(\x0b\x32:.keysight.dplane.ranger.l23access.id_type.v1.PacketGroupId\"z\n\x0cSizeBinsType\x12\x18\n\x10\x65nable_size_bins\x18\x01 \x01(\x08\x12P\n\tsize_bins\x18\x02 \x01(\x0b\x32=.keysight.dplane.ranger.l23access.packet_group.v1.SizeBinList\"\xe9\x0c\n\x11\x43onfigurationType\x12\x63\n\x14\x64\x65lay_variation_mode\x18\x01 \x01(\x0e\x32\x45.keysight.dplane.ranger.l23access.packet_group.v1.EDelayVariationMode\x12\x1b\n\x13\x65nable128k_bin_mode\x18\x02 \x01(\x08\x12\x1c\n\x14\x65nable_group_id_mask\x18\x03 \x01(\x08\x12\x1a\n\x12\x65nable_insert_pgid\x18\x04 \x01(\x08\x12\"\n\x1a\x65nable_last_bit_time_stamp\x18\x05 \x01(\x08\x12\x1b\n\x13\x65nable_latency_bins\x18\x06 \x01(\x08\x12&\n\x1e\x65nable_re_arm_first_time_stamp\x18\x07 \x01(\x08\x12\x18\n\x10\x65nable_rx_filter\x18\x08 \x01(\x08\x12\x1d\n\x15\x65nable_signature_mask\x18\t \x01(\x08\x12\x10\n\x08group_id\x18\n \x01(\x05\x12\x15\n\rgroup_id_mask\x18\x0b \x01(\x05\x12U\n\rgroup_id_mode\x18\x0c \x01(\x0e\x32>.keysight.dplane.ranger.l23access.packet_group.v1.EGroupIdMode\x12\x17\n\x0fgroup_id_offset\x18\r \x01(\x05\x12\x15\n\rheader_filter\x18\x0e \x01(\x0c\x12\x1a\n\x12header_filter_mask\x18\x0f \x01(\x0c\x12\x18\n\x10ignore_signature\x18\x10 \x01(\x08\x12`\n\x16update_time_stamp_mode\x18\x11 \x01(\x0e\x32@.keysight.dplane.ranger.l23access.packet_group.v1.ETimeStampMode\x12!\n\x19insert_sequence_signature\x18\x12 \x01(\x08\x12\x18\n\x10insert_signature\x18\x13 \x01(\x08\x12\x1b\n\x13ieee_preamble_match\x18\x14 \x01(\x08\x12V\n\x0clatency_bins\x18\x15 \x01(\x0b\x32@.keysight.dplane.ranger.l23access.packet_group.v1.LatencyBinList\x12T\n\x0csize_bins_rx\x18\x16 \x01(\x0b\x32>.keysight.dplane.ranger.l23access.packet_group.v1.SizeBinsType\x12Z\n\x0flatency_control\x18\x17 \x01(\x0e\x32\x41.keysight.dplane.ranger.l23access.packet_group.v1.ELatencyControl\x12\\\n\x10measurement_mode\x18\x18 \x01(\x0e\x32\x42.keysight.dplane.ranger.l23access.packet_group.v1.EMeasurementMode\x12\x15\n\rpreamble_size\x18\x19 \x01(\x05\x12\'\n\x1fseq_adv_tracking_late_threshold\x18\x1a \x01(\x05\x12 \n\x18sequence_error_threshold\x18\x1b \x01(\x05\x12g\n\x16sequence_checking_mode\x18\x1c \x01(\x0e\x32G.keysight.dplane.ranger.l23access.packet_group.v1.ESequenceCheckingMode\x12\x1e\n\x16sequence_number_offset\x18\x1d \x01(\x05\x12\x11\n\tsignature\x18\x1e \x01(\x0c\x12\x16\n\x0esignature_mask\x18\x1f \x01(\x0c\x12\x18\n\x10signature_offset\x18  \x01(\x05\x12\x63\n\x13split_packet_groups\x18! \x01(\x0b\x32\x46.keysight.dplane.ranger.l23access.packet_group.v1.SplitPacketGroupList\x12W\n\x0epgid_stat_mode\x18\" \x01(\x0e\x32?.keysight.dplane.ranger.l23access.packet_group.v1.EPGIDStatMode\"\x1f\n\x0eLatencyBinList\x12\r\n\x05items\x18\x01 \x03(\x01\"\x1c\n\x0bSizeBinList\x12\r\n\x05items\x18\x01 \x03(\x05\"p\n\x14SplitPacketGroupList\x12X\n\x05items\x18\x01 \x03(\x0b\x32I.keysight.dplane.ranger.l23access.split_packet_group.v1.ConfigurationType\"[\n\rPGIDRangeList\x12J\n\x05items\x18\x01 \x03(\x0b\x32;.keysight.dplane.ranger.l23access.packet_group.v1.PGIDRange*\xd1\x01\n\x13\x45\x44\x65layVariationMode\x12>\n:EDELAY_VARIATION_MODE_DELAY_VARIATION_WITH_SEQUENCE_ERRORS\x10\x00\x12>\n:EDELAY_VARIATION_MODE_DELAY_VARIATION_WITH_LATENCY_MIN_MAX\x10\x01\x12:\n6EDELAY_VARIATION_MODE_DELAY_VARIATION_WITH_LATENCY_AVG\x10\x02*\xe2\x01\n\x0c\x45GroupIdMode\x12&\n\"EGROUP_ID_MODE_PACKET_GROUP_CUSTOM\x10\x00\x12$\n EGROUP_ID_MODE_PACKET_GROUP_DSCP\x10\x01\x12\x33\n/EGROUP_ID_MODE_PACKET_GROUP_IP_V6_TRAFFIC_CLASS\x10\x02\x12(\n$EGROUP_ID_MODE_PACKET_GROUP_MPLS_EXP\x10\x03\x12%\n!EGROUP_ID_MODE_PACKET_GROUP_SPLIT\x10\x04*\xd5\x01\n\x0f\x45LatencyControl\x12 \n\x1c\x45LATENCY_CONTROL_CUT_THROUGH\x10\x00\x12&\n\"ELATENCY_CONTROL_STORE_AND_FORWARD\x10\x01\x12)\n%ELATENCY_CONTROL_INTER_ARRIVAL_JITTER\x10\x03\x12&\n\"ELATENCY_CONTROL_FIRST_IN_LAST_OUT\x10\x04\x12%\n!ELATENCY_CONTROL_LAST_IN_LAST_OUT\x10\x05*\xb8\x01\n\x10\x45MeasurementMode\x12/\n+EMEASUREMENT_MODE_PACKET_GROUP_MODE_LATENCY\x10\x00\x12:\n6EMEASUREMENT_MODE_PACKET_GROUP_MODE_INTER_ARRIVAL_TIME\x10\x01\x12\x37\n3EMEASUREMENT_MODE_PACKET_GROUP_MODE_DELAY_VARIATION\x10\x02*r\n\x0e\x45TimeStampMode\x12/\n+ETIME_STAMP_MODE_TIME_STAMP_FOR_ENTIRE_PGID\x10\x00\x12/\n+ETIME_STAMP_MODE_UPDATE_TIME_STAMP_ON_ERROR\x10\x01*\xa5\x01\n\x15\x45SequenceCheckingMode\x12)\n%ESEQUENCE_CHECKING_MODE_SEQ_THRESHOLD\x10\x00\x12\x33\n/ESEQUENCE_CHECKING_MODE_SEQ_MULTI_SWITCHED_PATH\x10\x01\x12,\n(ESEQUENCE_CHECKING_MODE_SEQ_ADV_TRACKING\x10\x02*X\n\rEPGIDStatMode\x12\"\n\x1e\x45PGID_STAT_MODE_K4_K_STAT_MODE\x10\x00\x12#\n\x1f\x45PGID_STAT_MODE_K32_K_STAT_MODE\x10\x01\x32\x14\n\x12PacketGroupServiceBzZugitlab.it.keysight.com/ranger/ranger-api-schema/pkg/api/keysight/dplane/ranger/l23access/packet_group/v1;packet_group\xf8\x01\x01\x62\x06proto3')

_EDELAYVARIATIONMODE = DESCRIPTOR.enum_types_by_name['EDelayVariationMode']
EDelayVariationMode = enum_type_wrapper.EnumTypeWrapper(_EDELAYVARIATIONMODE)
_EGROUPIDMODE = DESCRIPTOR.enum_types_by_name['EGroupIdMode']
EGroupIdMode = enum_type_wrapper.EnumTypeWrapper(_EGROUPIDMODE)
_ELATENCYCONTROL = DESCRIPTOR.enum_types_by_name['ELatencyControl']
ELatencyControl = enum_type_wrapper.EnumTypeWrapper(_ELATENCYCONTROL)
_EMEASUREMENTMODE = DESCRIPTOR.enum_types_by_name['EMeasurementMode']
EMeasurementMode = enum_type_wrapper.EnumTypeWrapper(_EMEASUREMENTMODE)
_ETIMESTAMPMODE = DESCRIPTOR.enum_types_by_name['ETimeStampMode']
ETimeStampMode = enum_type_wrapper.EnumTypeWrapper(_ETIMESTAMPMODE)
_ESEQUENCECHECKINGMODE = DESCRIPTOR.enum_types_by_name['ESequenceCheckingMode']
ESequenceCheckingMode = enum_type_wrapper.EnumTypeWrapper(_ESEQUENCECHECKINGMODE)
_EPGIDSTATMODE = DESCRIPTOR.enum_types_by_name['EPGIDStatMode']
EPGIDStatMode = enum_type_wrapper.EnumTypeWrapper(_EPGIDSTATMODE)
EDELAY_VARIATION_MODE_DELAY_VARIATION_WITH_SEQUENCE_ERRORS = 0
EDELAY_VARIATION_MODE_DELAY_VARIATION_WITH_LATENCY_MIN_MAX = 1
EDELAY_VARIATION_MODE_DELAY_VARIATION_WITH_LATENCY_AVG = 2
EGROUP_ID_MODE_PACKET_GROUP_CUSTOM = 0
EGROUP_ID_MODE_PACKET_GROUP_DSCP = 1
EGROUP_ID_MODE_PACKET_GROUP_IP_V6_TRAFFIC_CLASS = 2
EGROUP_ID_MODE_PACKET_GROUP_MPLS_EXP = 3
EGROUP_ID_MODE_PACKET_GROUP_SPLIT = 4
ELATENCY_CONTROL_CUT_THROUGH = 0
ELATENCY_CONTROL_STORE_AND_FORWARD = 1
ELATENCY_CONTROL_INTER_ARRIVAL_JITTER = 3
ELATENCY_CONTROL_FIRST_IN_LAST_OUT = 4
ELATENCY_CONTROL_LAST_IN_LAST_OUT = 5
EMEASUREMENT_MODE_PACKET_GROUP_MODE_LATENCY = 0
EMEASUREMENT_MODE_PACKET_GROUP_MODE_INTER_ARRIVAL_TIME = 1
EMEASUREMENT_MODE_PACKET_GROUP_MODE_DELAY_VARIATION = 2
ETIME_STAMP_MODE_TIME_STAMP_FOR_ENTIRE_PGID = 0
ETIME_STAMP_MODE_UPDATE_TIME_STAMP_ON_ERROR = 1
ESEQUENCE_CHECKING_MODE_SEQ_THRESHOLD = 0
ESEQUENCE_CHECKING_MODE_SEQ_MULTI_SWITCHED_PATH = 1
ESEQUENCE_CHECKING_MODE_SEQ_ADV_TRACKING = 2
EPGID_STAT_MODE_K4_K_STAT_MODE = 0
EPGID_STAT_MODE_K32_K_STAT_MODE = 1


_PGIDRANGE = DESCRIPTOR.message_types_by_name['PGIDRange']
_SIZEBINSTYPE = DESCRIPTOR.message_types_by_name['SizeBinsType']
_CONFIGURATIONTYPE = DESCRIPTOR.message_types_by_name['ConfigurationType']
_LATENCYBINLIST = DESCRIPTOR.message_types_by_name['LatencyBinList']
_SIZEBINLIST = DESCRIPTOR.message_types_by_name['SizeBinList']
_SPLITPACKETGROUPLIST = DESCRIPTOR.message_types_by_name['SplitPacketGroupList']
_PGIDRANGELIST = DESCRIPTOR.message_types_by_name['PGIDRangeList']
PGIDRange = _reflection.GeneratedProtocolMessageType('PGIDRange', (_message.Message,), {
  'DESCRIPTOR' : _PGIDRANGE,
  '__module__' : 'keysight.dplane.ranger.l23access.packet_group.v1.packet_group_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.packet_group.v1.PGIDRange)
  })
_sym_db.RegisterMessage(PGIDRange)

SizeBinsType = _reflection.GeneratedProtocolMessageType('SizeBinsType', (_message.Message,), {
  'DESCRIPTOR' : _SIZEBINSTYPE,
  '__module__' : 'keysight.dplane.ranger.l23access.packet_group.v1.packet_group_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.packet_group.v1.SizeBinsType)
  })
_sym_db.RegisterMessage(SizeBinsType)

ConfigurationType = _reflection.GeneratedProtocolMessageType('ConfigurationType', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGURATIONTYPE,
  '__module__' : 'keysight.dplane.ranger.l23access.packet_group.v1.packet_group_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.packet_group.v1.ConfigurationType)
  })
_sym_db.RegisterMessage(ConfigurationType)

LatencyBinList = _reflection.GeneratedProtocolMessageType('LatencyBinList', (_message.Message,), {
  'DESCRIPTOR' : _LATENCYBINLIST,
  '__module__' : 'keysight.dplane.ranger.l23access.packet_group.v1.packet_group_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.packet_group.v1.LatencyBinList)
  })
_sym_db.RegisterMessage(LatencyBinList)

SizeBinList = _reflection.GeneratedProtocolMessageType('SizeBinList', (_message.Message,), {
  'DESCRIPTOR' : _SIZEBINLIST,
  '__module__' : 'keysight.dplane.ranger.l23access.packet_group.v1.packet_group_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.packet_group.v1.SizeBinList)
  })
_sym_db.RegisterMessage(SizeBinList)

SplitPacketGroupList = _reflection.GeneratedProtocolMessageType('SplitPacketGroupList', (_message.Message,), {
  'DESCRIPTOR' : _SPLITPACKETGROUPLIST,
  '__module__' : 'keysight.dplane.ranger.l23access.packet_group.v1.packet_group_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.packet_group.v1.SplitPacketGroupList)
  })
_sym_db.RegisterMessage(SplitPacketGroupList)

PGIDRangeList = _reflection.GeneratedProtocolMessageType('PGIDRangeList', (_message.Message,), {
  'DESCRIPTOR' : _PGIDRANGELIST,
  '__module__' : 'keysight.dplane.ranger.l23access.packet_group.v1.packet_group_pb2'
  # @@protoc_insertion_point(class_scope:keysight.dplane.ranger.l23access.packet_group.v1.PGIDRangeList)
  })
_sym_db.RegisterMessage(PGIDRangeList)

_PACKETGROUPSERVICE = DESCRIPTOR.services_by_name['PacketGroupService']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'Zugitlab.it.keysight.com/ranger/ranger-api-schema/pkg/api/keysight/dplane/ranger/l23access/packet_group/v1;packet_group\370\001\001'
  _EDELAYVARIATIONMODE._serialized_start=2462
  _EDELAYVARIATIONMODE._serialized_end=2671
  _EGROUPIDMODE._serialized_start=2674
  _EGROUPIDMODE._serialized_end=2900
  _ELATENCYCONTROL._serialized_start=2903
  _ELATENCYCONTROL._serialized_end=3116
  _EMEASUREMENTMODE._serialized_start=3119
  _EMEASUREMENTMODE._serialized_end=3303
  _ETIMESTAMPMODE._serialized_start=3305
  _ETIMESTAMPMODE._serialized_end=3419
  _ESEQUENCECHECKINGMODE._serialized_start=3422
  _ESEQUENCECHECKINGMODE._serialized_end=3587
  _EPGIDSTATMODE._serialized_start=3589
  _EPGIDSTATMODE._serialized_end=3677
  _PGIDRANGE._serialized_start=262
  _PGIDRANGE._serialized_end=421
  _SIZEBINSTYPE._serialized_start=423
  _SIZEBINSTYPE._serialized_end=545
  _CONFIGURATIONTYPE._serialized_start=548
  _CONFIGURATIONTYPE._serialized_end=2189
  _LATENCYBINLIST._serialized_start=2191
  _LATENCYBINLIST._serialized_end=2222
  _SIZEBINLIST._serialized_start=2224
  _SIZEBINLIST._serialized_end=2252
  _SPLITPACKETGROUPLIST._serialized_start=2254
  _SPLITPACKETGROUPLIST._serialized_end=2366
  _PGIDRANGELIST._serialized_start=2368
  _PGIDRANGELIST._serialized_end=2459
  _PACKETGROUPSERVICE._serialized_start=3679
  _PACKETGROUPSERVICE._serialized_end=3699
# @@protoc_insertion_point(module_scope)
